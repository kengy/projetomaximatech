package com.kengy.projetomaximatech

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.core.graphics.drawable.toDrawable
import com.kengy.projetomaximatech.Controller.ContCliente
import com.kengy.projetomaximatech.util.BottomNavigateActivity
import com.kengy.projetomaximatech.util.NetWorkVerification
import com.kengy.projetomaximatech.util.tetes.SwipeActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_swipe.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    var cardCliente : CardView?  = null
    var imgNetWork  : ImageView? = null
    var cardpedidos : CardView? = null


    private  lateinit var mRunnable  : Runnable
    private lateinit var mHandler : Handler



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cardpedidos = card_pedidos
         cardCliente = card_cliente
         imgNetWork = img_network

        atualizaStatusConexao ()


        cardCliente?.setOnClickListener(this)
        cardpedidos?.setOnClickListener(this)



        mHandler = Handler()

        swipe_layout_mainActivity.setOnRefreshListener {
            // Initialize a new Runnable
            mRunnable = Runnable {
                // Update the text view text with a random number
                atualizaStatusConexao ()
                // Hide swipe to refresh icon animation
                swipe_layout_mainActivity.isRefreshing = false
            }

            // Execute the task after specified time
            mHandler.postDelayed(
                mRunnable,
                (1*1000).toLong() // Delay 1 to 5 seconds
            )
        }



    }


    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)

        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }


    private fun atualizaStatusConexao () {

        if (isNetworkAvailable()) {
            imgNetWork?.setImageResource(R.mipmap.ic_maxima_nuvem_conectado)
        }else
            imgNetWork?.setImageResource(R.mipmap.ic_maxima_nuvem_desconectado)

    }


    override fun onClick(view: View?) {

        if (view != null) {

            when (view.id){
                card_cliente.id -> {
                    var it = Intent(this, BottomNavigateActivity::class.java)
                        startActivity(it)
                }

                card_pedidos.id -> {
                    var it = Intent(this, SwipeActivity::class.java)
                    startActivity(it)
                }
            }
        }

    }


}
