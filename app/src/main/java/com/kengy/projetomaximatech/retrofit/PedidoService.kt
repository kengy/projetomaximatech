package com.kengy.projetomaximatech.retrofit

import com.kengy.projetomaximatech.Model.myPedidos
import retrofit2.Call
import retrofit2.http.GET

interface PedidoService {

    @GET("wjl97")
    fun obtemPedidos(): Call<myPedidos>
}