package com.kengy.projetomaximatech.retrofit

import com.kengy.projetomaximatech.Model.myCliente
import retrofit2.Call
import retrofit2.http.GET


interface ClienteServices {

    @GET("1bo7qj")
    fun obtemCliente():Call<myCliente>

    @GET ("wjl97")
    fun obtemPedidoe():Call<myCliente>
}