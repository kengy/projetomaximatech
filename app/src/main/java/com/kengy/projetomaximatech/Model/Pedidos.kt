package com.kengy.projetomaximatech.Model

import com.google.gson.annotations.SerializedName

class Pedidos(

    var numero_ped_Rca: Int,
    var numero_ped_erp: String,
    var codigoCliente: String,
    var NOMECLIENTE: String,
    var data: String,
    var status: String,
    var critica: String,
    var tipo: String,
    var legendas : List<String>
)


data class myPedidos(var pedidos: List<Pedidos>)

enum class Legenda {
    PEDIDO_CANCELADO_ERP,
    PEDIDO_SOFREU_CORTE,
    PEDIDO_FEITO_TELEMARKETING
}