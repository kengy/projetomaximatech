package com.kengy.projetomaximatech.util.ui.dados

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kengy.projetomaximatech.Controller.ContCliente
import com.kengy.projetomaximatech.Model.Cliente
import com.kengy.projetomaximatech.Model.Contatos
import com.kengy.projetomaximatech.Model.myCliente
import com.kengy.projetomaximatech.R
import com.kengy.projetomaximatech.retrofit.ClienteServices
import com.kengy.projetomaximatech.retrofit.RetrofitInitializer
import com.kengy.projetomaximatech.util.ui.ContatosAdapter
import kotlinx.android.synthetic.main.fragment_dados.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var mContCliente: ContCliente
    var cliente: Cliente? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_dados, container, false)

        return root


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        obtemClienteApi()
    }


    fun obtemClienteApi() {

        val url: String = "https://api.myjson.com/bins/"
        val retrofit: Retrofit = RetrofitInitializer().getRetrofitInstance(url)
        val endpoint = retrofit.create(ClienteServices::class.java)
        val callback = endpoint.obtemCliente()

        callback.enqueue(object : Callback<myCliente> {

            override fun onResponse(call: Call<myCliente>, response: Response<myCliente>) {

                iniciaCampos(response.body()!!.cliente)
                populaContatos(response.body()!!.cliente.contatos)

            }

            override fun onFailure(call: Call<myCliente>, t: Throwable) {

                Toast.makeText(activity, "Deu bosta ao pegar", Toast.LENGTH_LONG)
            }
        })
    }


    fun populaContatos(contatos: List<Contatos>) {

       // val recycleContatos = recy_contatos

        recy_contatos.adapter = ContatosAdapter(contatos)

        recy_contatos.layoutManager = LinearLayoutManager(activity)
    }

    fun iniciaCampos(cliente: Cliente) {

        txt_razao_social.text = cliente.razao_social
        txt_fantasia.text = cliente.nomeFantasia
        txt_cpf.text = cliente.cpf
        txt_cnpj.text = cliente.cnpj
        txt_ramo_atividade.text = cliente.ramo_atividade
        txtt_endereco.text = cliente.endereco
    }
}