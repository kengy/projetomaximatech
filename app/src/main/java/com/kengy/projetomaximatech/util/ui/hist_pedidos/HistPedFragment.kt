package com.kengy.projetomaximatech.util.ui.hist_pedidos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.kengy.projetomaximatech.Model.Pedidos
import com.kengy.projetomaximatech.Model.myPedidos
import com.kengy.projetomaximatech.R
import com.kengy.projetomaximatech.retrofit.ClienteServices
import com.kengy.projetomaximatech.retrofit.PedidoService
import com.kengy.projetomaximatech.retrofit.RetrofitInitializer
import com.kengy.projetomaximatech.util.ui.PedidosAdapter
import kotlinx.android.synthetic.main.fragment_hist_pedidos.*
import kotlinx.android.synthetic.main.fragment_hist_pedidos.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


class HistPedFragment : Fragment() {

    private lateinit var dashboardViewModel: HistPedViewModel

    val rv: RecyclerView
        get() = this.recy_hist_pedidos

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(HistPedViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_hist_pedidos, container, false)
//        val textView: TextView = root.findViewById(R.id.text_dashboard)
//        dashboardViewModel.text.observe(this, Observer {
//            textView.text = it
        //   })

        obtemClienteApi(root)

        return root

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        

       // populaCampos(retornaPedido())

    }

    private fun populaCampos(pedidos: List<Pedidos>,root:View) {



        root.recy_hist_pedidos.adapter = PedidosAdapter(pedidos)
        root.recy_hist_pedidos.layoutManager = LinearLayoutManager(activity)


    }


    fun retornaPedido() : List<Pedidos>{

        val strjSON = """
  [
    {
      "numero_ped_Rca": 542180,
      "numero_ped_erp": "6421800987",
      "codigoCliente": "848.246",
      "NOMECLIENTE": "Burger",
      "data": "2018-09-06T10:12:21-0300",
      "status": "Processado",
      "critica": "SUCESSO",
      "tipo": "ORCAMENTO",
      "legendas": [
        "PEDIDO_SOFREU_CORTE",
        "PEDIDO_FEITO_TELEMARKETING"
      ]
    },
    {
      "numero_ped_Rca": 542181,
      "numero_ped_erp": "6421800988",
      "codigoCliente": "8.246",
      "NOMECLIENTE": "Dona Dona",
      "data": "2018-09-06T16:12:21-0300",
      "status": "Em processamento",
      "tipo": "PEDIDO",
      "legendas": []
    },
    {
      "numero_ped_Rca": 542182,
      "numero_ped_erp": "6421800989",
      "codigoCliente": "1.236",
      "NOMECLIENTE": "Supermercado do Zé",
      "data": "2018-09-06T17:12:21-0300",
      "status": "Processado",
      "critica": "FALHA_PARCIAL",
      "tipo": "PEDIDO",
      "legendas": [
        "PEDIDO_CANCELADO_ERP"
      ]
    },
    {
      "numero_ped_Rca": 542183,
      "numero_ped_erp": "6421800990",
      "codigoCliente": "1.115",
      "NOMECLIENTE": "Armazém da Carne",
      "data": "2018-09-06T08:22:11-0300",
      "status": "Pendente",
      "tipo": "PEDIDO"
    },
    {
      "numero_ped_Rca": 542184,
      "numero_ped_erp": "6421800991",
      "codigoCliente": "2.4459",
      "NOMECLIENTE": "Atacado Novo",
      "data": "2018-09-06T08:21:11-0300",
      "status": "Pendente",
      "tipo": "PEDIDO"
    },
    {
      "numero_ped_Rca": 542185,
      "numero_ped_erp": "6421800992",
      "codigoCliente": "965.123",
      "NOMECLIENTE": "Burger",
      "data": "2018-09-05T09:31:18-0300",
      "status": "Processado",
      "critica": "SUCESSO",
      "tipo": "PEDIDO",
      "legendas": [
        "PEDIDO_SOFREU_CORTE",
        "PEDIDO_FEITO_TELEMARKETING"
      ]
    },
    {
      "numero_ped_Rca": 542186,
      "numero_ped_erp": "6421800993",
      "codigoCliente": "2.4459",
      "NOMECLIENTE": "Atacadão dos Preços baixos",
      "data": "2018-09-04T15:23:59-0300",
      "status": "Pendente",
      "tipo": "ORCAMENTO"
    },
    {
      "numero_ped_Rca": 542187,
      "numero_ped_erp": "6421800994",
      "codigoCliente": "3",
      "NOMECLIENTE": "Padaria do Zé Borges",
      "data": "2018-09-03T18:30:49-0300",
      "status": "Pendente",
      "tipo": "ORCAMENTO"
    },
    {
      "numero_ped_Rca": 542188,
      "numero_ped_erp": "6421800995",
      "codigoCliente": "3",
      "NOMECLIENTE": "Supermercado Braga",
      "data": "2018-09-11T12:30:19-0300",
      "status": "Em Processamento",
      "tipo": "PEDIDO"
    },
    {
      "numero_ped_Rca": 542189,
      "numero_ped_erp": "6421800996",
      "codigoCliente": "965",
      "NOMECLIENTE": "Supermecado Giro Mais",
      "data": "2018-09-12T08:33:18-0300",
      "status": "Processado",
      "critica": "SUCESSO",
      "tipo": "PEDIDO"
    }
  ]
"""

        var  gson = GsonBuilder().setPrettyPrinting().create()
      //  var pedidos = g.fromJson(strjSON, Pedidos::class.java)


       var pedidos: List<Pedidos>  =gson.fromJson(strjSON, object : TypeToken<List<Pedidos>>() {}.type)


        return pedidos
    }


    fun obtemClienteApi(root:View) {

        val url: String = "https://api.myjson.com/bins/"
        val retrofit: Retrofit = RetrofitInitializer().getRetrofitInstance(url)
        val endpoint = retrofit.create(PedidoService::class.java)
        val callback = endpoint.obtemPedidos()

        callback.enqueue(object : Callback<myPedidos> {

            override fun onResponse(call: Call<myPedidos>, response: Response<myPedidos>) {
                populaCampos(response.body()!!.pedidos,root)

            }

            override fun onFailure(call: Call<myPedidos>, t: Throwable) {

                Toast.makeText(activity, "Deu bosta ao pegar", Toast.LENGTH_LONG)
            }
        })
    }







}