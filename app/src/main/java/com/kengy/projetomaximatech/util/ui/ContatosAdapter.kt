package com.kengy.projetomaximatech.util.ui


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kengy.projetomaximatech.Model.Contatos
import com.kengy.projetomaximatech.R
import kotlinx.android.synthetic.main.contato_adapter.view.*


class ContatosAdapter(
    private val contatos: List<Contatos>
) : RecyclerView.Adapter<ContatosAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contato_adapter, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contatos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val contato = contatos[position]
        holder.bindView(contato)

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {




        fun bindView(contatos: Contatos) {

            val nome = itemView.txt_nome_contato
            val telefone = itemView.txt_telefone
            val celular = itemView.txt_celular
            val conjugue = itemView.txt_conjugue
            val tipo = itemView.txt_tipo
            val time = itemView.txt_time
            val time2 = itemView.txt_time2
            val email = itemView.txt_emal
            val dtNascimento = itemView.txt_dt_nascimento
            val dtNascConjugue = itemView.txt_dt_nasc_conjugue

            nome.text = contatos.nome
            telefone.text = contatos.telefone
            celular.text = contatos.celular
            conjugue.text = contatos.conjuge
            tipo.text = contatos.tipo
            time.text = contatos.time
            time2.text = contatos.time
            email.text = contatos.e_mail
            dtNascimento.text = contatos.data_nascimento
            dtNascConjugue.text = contatos.dataNascimentoConjuge

        }

    }
}

