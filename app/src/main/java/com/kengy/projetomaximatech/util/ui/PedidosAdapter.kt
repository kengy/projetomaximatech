package com.kengy.projetomaximatech.util.ui

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.kengy.projetomaximatech.Model.Legendas
import com.kengy.projetomaximatech.Model.Pedidos
import com.kengy.projetomaximatech.R
import kotlinx.android.synthetic.main.activity_swipe.*
import kotlinx.android.synthetic.main.adapter_hist_pedidos.view.*
import kotlinx.android.synthetic.main.contato_adapter.view.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


class PedidosAdapter(private val pedidos: List<Pedidos>) :
    RecyclerView.Adapter<PedidosAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_hist_pedidos, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return pedidos.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindData(pedidos[position])

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bindData(pedido: Pedidos) {


            var letra: String = pedido.NOMECLIENTE.first().toString()

            var generator: ColorGenerator = ColorGenerator.MATERIAL; // or use DEFAULT

            var color = generator.getRandomColor();

            val drawable = TextDrawable.builder()
                .buildRound(letra, color)

            val image = itemView.img_letra

            image.setImageDrawable(drawable)


            itemView.txt_numped_rca_erp.text =
                " ${pedido.numero_ped_Rca} / ${pedido.numero_ped_erp}"
            itemView.txt_cliente.text = "${pedido.codigoCliente} - ${pedido.NOMECLIENTE}"
            itemView.txt_status.text = pedido.status



            if (pedido.critica == null) {
                itemView.img_critica.setImageResource(R.mipmap.ic_maxima_aguardando_critica)
            } else
                if (pedido.critica.equals("ALERTA")) {
                    itemView.img_critica.setImageResource(R.mipmap.ic_maxima_critica_alerta)

                } else if (pedido.critica.equals("SUCESSO")) {
                    itemView.img_critica.setImageResource(R.mipmap.ic_maxima_critica_sucesso)

                }


            if (pedido.legendas != null) {

                if (pedido.legendas.size != 0 && pedido.legendas != null) {
                    when (pedido.legendas[pedido.legendas.size - 1]) {

                        "PEDIDO_CANCELADO_ERP" -> itemView.img_legenda.setImageResource(R.mipmap.ic_maxima_legenda_cancelamento)
                        "PEDIDO_SOFREU_CORTE" -> itemView.img_legenda.setImageResource(R.mipmap.ic_maxima_legenda_corte)
                        "PEDIDO_FEITO_TELEMARKETING" -> itemView.img_legenda.setImageResource(R.mipmap.ic_maxima_legenda_telemarketing)
                    }


                }
            } else {
                itemView.img_legenda.setImageResource(R.mipmap.ic_legenda_campanha)
            }

            itemView.txt_data_pedido.text =  formatterDate(pedido.data)


        }


        fun formatterDate(strDate : String): String{




         //   var srtFormated =  strDate.replace("T"," ").replaceRange(19..23,"") "2018-09-11T12:30:19-0300"
         //   val date = LocalDate.parse(srtFormated, DateTimeFormatter.ISO_OFFSET_DATE_TIME)

            var data = strDate.substring(0..9)
            var hora = strDate.substring(11..15)




            var formatoDataAtual : SimpleDateFormat = SimpleDateFormat("yyyy-mm-dd")

            var dataAtual : Date = Date()

            var dataFormated = formatoDataAtual.format(dataAtual)


            if (data.equals(dataFormated.toString()))
                return hora

            return data

        }




    }

}