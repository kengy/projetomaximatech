package com.kengy.projetomaximatech.util

import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.kengy.projetomaximatech.Controller.ContCliente
import com.kengy.projetomaximatech.Model.Cliente
import com.kengy.projetomaximatech.R
import com.kengy.projetomaximatech.util.ui.FragDialogLegendas

class BottomNavigateActivity : AppCompatActivity() {


    var contCliente = ContCliente()
    var cli: Cliente? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigate)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_hist_ped, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.menu_legendas -> {
                Toast.makeText(this, "Legenda Pressionada", Toast.LENGTH_LONG).show()

                //callDialogLegendas()
                callMyFragDialog()
                return true
            }
            else -> return super.onOptionsItemSelected(item)

        }


    }

    fun callMyFragDialog () {

        var diaFrag : FragDialogLegendas = FragDialogLegendas()

        diaFrag.show(supportFragmentManager,"teste")
    }

    fun callDialogLegendas() {

        var alertDialog = AlertDialog.Builder(this)

        alertDialog.setTitle("Legendas")
            .setMessage("Vai rolar altas paradas nesse Dialog").setCancelable(false)


        alertDialog.setPositiveButton("GG",{test, teste->


            Toast.makeText(this,"Yes lick  me",Toast.LENGTH_LONG)

        })

        alertDialog.show()

    }


}
