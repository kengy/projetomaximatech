package com.kengy.projetomaximatech.util.tetes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import kotlinx.android.synthetic.main.activity_swipe.*
import android.R
import android.graphics.Color
import android.widget.ImageView
import com.amulyakhare.textdrawable.TextDrawable


class SwipeActivity : AppCompatActivity() {

    private  lateinit var mRunnable  : Runnable
    private lateinit var mHandler : Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.kengy.projetomaximatech.R.layout.activity_swipe)

        mHandler = Handler()

        swipe_layout.setOnRefreshListener {
            // Initialize a new Runnable
            mRunnable = Runnable {
                // Update the text view text with a random number
                txt_teste_swipe.text = "Minha frase foi Atualizada"



                // Hide swipe to refresh icon animation
                swipe_layout.isRefreshing = false
            }

            // Execute the task after specified time
            mHandler.postDelayed(
                mRunnable,
                (3*1000).toLong() // Delay 1 to 5 seconds
            )
        }


        val drawable = TextDrawable.builder()
            .buildRect("A", Color.RED)

        val image = image_view
        image.setImageDrawable(drawable)


    }
}
