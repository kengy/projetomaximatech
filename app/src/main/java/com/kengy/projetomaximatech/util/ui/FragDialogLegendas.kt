package com.kengy.projetomaximatech.util.ui

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.kengy.projetomaximatech.R
import kotlinx.android.synthetic.main.activity_bottom_navigate.*
import kotlinx.android.synthetic.main.adapter_hist_pedidos.view.*
import kotlinx.android.synthetic.main.fragment_dialog_legendas.view.*

@Suppress("UNREACHABLE_CODE")
open class FragDialogLegendas : DialogFragment() {



    var imgPosiPend: ImageView? = null
    var imgPosiBloq: ImageView? = null
    val imgPosiLiberado: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        val view: View = activity?.layoutInflater!!.inflate(R.layout.fragment_dialog_legendas, null)

        var imgPedRecusado: ImageView? = null

      imgPedRecusado = view.findViewById(R.id.img_dialog_leg_pedRecusado)



        var generator: ColorGenerator = ColorGenerator.MATERIAL; // or use DEFAULT

        var color = generator.getRandomColor();

        val drawable = TextDrawable.builder()
            .buildRound("!", color)

      imgPedRecusado.setImageDrawable(drawable      ))
//        this.imgPosiBloq = view.findViewById(R.id.img_dialog_leg_posiBloq)
//        this.imgPosiPend = view.findViewById(R.id.img_dialog_leg_posiPendente)
/*
        view.img_dialog_leg_pedRecusado.setImageDrawable(
            setLetraDrewable(
                "!",
                hexConverterToColor("#ffa500")
            )
        )
        view.img_dialog_leg_posiBloq.setImageDrawable(
            setLetraDrewable(
                "B",
                hexConverterToColor("#1d38d1")
            )
        )
        view.img_dialog_leg_posiPendente.setImageDrawable(
            setLetraDrewable(
                "P",
                hexConverterToColor("#808080")
            )
        )
*/

        val dialog = AlertDialog.Builder(activity)
        dialog.setPositiveButton("Ok",{teste,teste1 ->


        })

        dialog.setCancelable(true)
        dialog.setView(view)

        return dialog.create()
    }


    fun setLetraDrewable(letra: String, color: Int): TextDrawable {

        var drawable = TextDrawable.builder().buildRound(letra, color)

        return drawable

    }

    fun hexConverterToColor(hex: String): Int {

        var color = Color.parseColor(hex)
        return color
    }
}